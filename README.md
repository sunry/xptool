# xptool

#### 介绍

P5 激活ADB工具

使用于P5 3.5(没有实测过)与3.6.1版本获取adb权限，前提是已经安装了termux软件

#### 安装教程

1. 在termux中输入 
   
   ```bash
   curl https://gitee.com/yzf_Air/xptool/raw/master/xptool -o xptool
   ```

2. 加权限
   
   ```bash
   chmod 777 xptool
   ```

#### 使用说明

1. 运行
   
   ```bash
   ./xptool
   ```

#### 常见问题

1.提示ADB安装失败

```bash
pkg install android-tools -y
```

在termux中手动安装一下，再重新运行xptool

#### 打赏作者

![image](https://gitee.com/yzf_Air/xptool/raw/master/donate.png)
